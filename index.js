// import node module
var Mocha = require("mocha");
var path = require("path");

// Run mocha executable
try {
  var mocha = new Mocha();
  mocha.addFile(path.join("./parser", "parser.test.js"));
  mocha.run();
} catch (e) {
  console.log(e);
}
