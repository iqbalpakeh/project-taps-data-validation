// import node module
const cheerio = require("cheerio");
const path = require("path");
const fs = require("fs");

// import my module
const debug = require("../log");

try {
  // Read taps html
  var html = fs.readFileSync(
    path.resolve(__dirname, "../data/data-sample.html"),
    "utf8"
  );
  var $ = cheerio.load(html);
  var document = {
    XTResult: [],
    XTError: []
  };

  // table
  $("#taps-interoperability-result-table tbody tr").each((i, el) => {
    $(el)
      .children()
      .each((ii, eel) => {
        if (ii == 1) {
          document.XTResult.push({
            vtf: $(eel)
              .text()
              .trim()
          });
        }
      });
    $(el)
      .children()
      .each((ii, eel) => {
        if (ii == 3) {
          document.XTResult[i].result = $(eel)
            .text()
            .trim();
        }
        if (ii == 5) {
          document.XTResult[i].observation = $(eel)
            .text()
            .trim();
        }
        if (ii == 6) {
          document.XTResult[i].failures = $(eel)
            .text()
            .trim()
            .split(" ")
            .join("")
            .split("\n")
            .join("")
            .split(",")
            .join(", ");
        }
        if (ii == 7) {
          document.XTResult[i].issue = $(eel)
            .text()
            .trim();
        }
        if (ii == 8) {
          document.XTResult[i].valid = $(eel)
            .text()
            .trim();
        }
      });
  });

  // table query
  $("#accordion-body-Result-Validation-Error .panel-body p").each((i, el) => {
    // add table
    document.XTError.push({
      [`table#${i}`]: $(el)
        .text()
        .trim(),
      vtfs: []
    });
    // add vtf to each table
    $(el)
      .next()
      .children()
      .each((ii, eel) => {
        document.XTError[i].vtfs.push({
          [`vtf#${ii}`]: $(eel)
            .text()
            .trim()
        });
      });
  });

  debug.log(document);
  fs.writeFileSync("data.json", JSON.stringify(document));

  // Catch any error while reading from data
} catch (e) {
  console.log(e);
}
