// import node module
const cheerio = require("cheerio");

// import my module
const debug = require("../log");

module.exports.extract = html => {
  var $ = cheerio.load(html);
  var document = {
    XTResult: [],
    XTError: []
  };

  // extrct cross-test result
  $("#taps-interoperability-result-table tbody tr").each((i, el) => {
    $(el)
      .children()
      .each((ii, eel) => {
        if (ii == 1) {
          document.XTResult.push({
            vtf: $(eel)
              .text()
              .trim()
          });
        }
      });
    $(el)
      .children()
      .each((ii, eel) => {
        if (ii == 3) {
          document.XTResult[i].result = $(eel)
            .text()
            .trim();
        }
        if (ii == 5) {
          document.XTResult[i].observation = $(eel)
            .text()
            .trim();
        }
        if (ii == 6) {
          document.XTResult[i].failures = $(eel)
            .text()
            .trim()
            .split(" ")
            .join("")
            .split("\n")
            .join("")
            .split(",")
            .join(", ");
        }
        if (ii == 7) {
          document.XTResult[i].analysis = $(eel)
            .text()
            .trim();
        }
        if (ii == 8) {
          document.XTResult[i].flag = $(eel)
            .text()
            .trim();
        }
      });
  });

  // extract validation result table
  $("#accordion-body-Result-Validation-Error .panel-body p").each((i, el) => {
    // add table
    document.XTError.push({
      tableName: $(el)
        .text()
        .trim(),
      vtfs: []
    });
    // add vtf to each table
    $(el)
      .next()
      .children()
      .each((ii, eel) => {
        document.XTError[i].vtfs.push({
          vtf: $(eel)
            .text()
            .trim()
        });
      });
  });

  // return the extracted document
  return document;
};
