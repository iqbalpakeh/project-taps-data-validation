// import node module
const expect = require("expect");

// import my module
const debug = require("../log");
const parser = require("./parser");

// Check each vtf configuration
parser.tests.map(test => {
  // Get the specific xt-result for each vtf
  var xtr = parser.document.XTResult.filter(xtr => xtr.vtf === test.vtf)[0];

  describe(`${test.vtf}`, () => {
    // Check RESULT column
    if (test.result) {
      it(`RESULT should be ${test.result}`, () => {
        expect(xtr.result).toBe(test.result);
      });
    } else {
      it(`RESULT should be empty`, () => {
        expect(xtr.result).toBe("");
      })
    }

    // Check OBSERVATION column
    if (test.observation) {
      it(`OBSERVATION should contain comment`, () => {
        expect(xtr.observation).toNotBe("");
      });
    } else {
      it(`OBSERVATION should be empty`, () => {
        expect(xtr.observation).toBe("");
      });
    }

    // Check FAILURES column
    if (test.failures) {
      it(`FAILURES should be ${test.failures}`, () => {
        expect(xtr.failures).toBe(test.failures);
      });
    } else {
      it(`FAILURES should be empty`, () => {
        expect(xtr.failures).toBe("");
      });
    }

    // Check ANALYSIS column
    if (test.analysis) {
      it(`ANALYSIS should contain comment`, () => {
        expect(xtr.analysis).toNotBe("");
      });
    } else {
      it(`ANALYSIS should be empty`, () => {
        expect(xtr.analysis).toBe("");
      });
    }

    // Check RESULT-FLAG column
    it(`RESULT-FLAG should be ${test.flag}`, () => {
      expect(xtr.flag).toBe(test.flag);
    });

    // Check tables
    test.tables.map(table => {
      if (table.listed) {
        // Table should exist
        it(`Table "${table.name}" should exist`, () => {
          let length = parser.document.XTError.filter(
            xtTable => xtTable.tableName === table.name
          ).length;
          expect(length).toBe(1);
        });
        // VTF should be listed on the table
        it(`${test.vtf} should be listed on table "${table.name}"`, () => {
          let length = parser.document.XTError.filter(
            xtTable => xtTable.tableName === table.name
          )[0].vtfs.filter(vtf => vtf.vtf === test.vtf).length;
          expect(length).toBe(1);
        });
      } else {
        // If table not exist, means ok
        let length = parser.document.XTError.filter(
          xtTable => xtTable.tableName === table.name
        ).length;
        if (length == 1) {
          // If table exist, it should not be listed
          it(`${test.vtf} should NOT be listed on table "${
            table.name
          }"`, () => {
            let length = parser.document.XTError.filter(
              xtTable => xtTable.tableName === table.name
            )[0].vtfs.filter(vtf => vtf.vtf === test.vtf).length;
            expect(length).toBe(0);
          });
        }
      }
    });
  });
});

// Check extra tables
describe("Check extra table", () => {
  parser.document.XTError.map(xtTable => {
    it(`Table "${xtTable.tableName}" is on the specification`, () => {
      let length = parser.tests[0].tables.filter(
        table => table.name === xtTable.tableName
      ).length;
      expect(length).toBe(1);
    });
  });
});
