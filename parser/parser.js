// import node module
const path = require("path");
const fs = require("fs");

// import my module
const debug = require("../log");
const extractor = require("./extractor");

try {
  // Read config.json
  var json = fs.readFileSync(
    path.resolve(__dirname, "../data/config.json"),
    "utf8"
  );
  var config = JSON.parse(json);

  // Read taps html
  var html = fs.readFileSync(
    path.resolve(__dirname, "../data/", config.html),
    "utf8"
  );
  var document = extractor.extract(html);

  // Write DOM
  fs.writeFileSync(
    path.resolve(__dirname, "../data/", config.dom),
    JSON.stringify(document, undefined, 2)
  );

  // Read test scenario
  var json = fs.readFileSync(
    path.resolve(__dirname, "../data/", config.scenario),
    "utf8"
  );
  var tests = JSON.parse(json);

  // Catch any error while reading from data
} catch (e) {
  console.log(e);
}

// Export to other module
module.exports = {
  document,
  tests
};
