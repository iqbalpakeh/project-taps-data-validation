// import node module
const util = require("util");
const sym = require("log-symbols");
const chalk = require("chalk");

/**
 * control debug log visibility
 */
module.exports.log = object => {
  if ((debug = true)) {
    console.log(chalk.yellow(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> S"));
    console.log(
      chalk.yellow(util.inspect(object, { showHidden: false, depth: null }))
    );
    console.log(chalk.yellow(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> E"));
  }
};
